<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use SoftDeletes;
    
    protected $fillable = ['nama','nrp', 'email', 'jurusan']; // Linked to Controller: StudentsController.php
    // fillable = boleh di isi                                  // Linked to Controller: StudentsController.php
    // guarded = tidak boleh di isi                             // Linked to Controller: StudentsController.php
    
}

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'PagesController@home');        //php artisan make:Controller
// Route::get('/about', 'PagesController@about');

// Route::get('/mahasiswa', 'MahasiswaController@index'); //php artisan make:controller MahasiswaController --resource
// // Students
// Route::get('/students', 'StudentsController@index'); //php artisan make:controller MahasiswaController --resource
// Route::get('/students/create', 'StudentsController@create'); 
// Route::get('/students/{student}', 'StudentsController@show'); //php artisan make:controller MahasiswaController -r -m
// Route::post('/students', 'StudentsController@store');
// Route::delete('/students/{student}', 'StudentsController@destroy');
// Route::get('/students/{student}/edit', 'StudentsController@edit');
// Route::patch('/students/{student}', 'StudentsController@update');
Route::resource('students', 'studentsController');

// Route::get('/', function (){
//     return view('welcome');
// });